//
//  PSDLayer.h
//  ScreenToLayers
//
//  Forked from : https://github.com/bengotow/PSDWriter
//  This software is released subject to licensing conditions as detailed in Licence.txt.
//

#import <Foundation/Foundation.h>

@interface PSDLayer : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSData *imageData;
@property (nonatomic, assign) CGRect rect;
@property (readonly , assign) CGRect floorRect;

@end

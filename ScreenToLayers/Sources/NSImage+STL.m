//
//  NSImage+STL.m
//  ScreenToLayers
//
//  Created by Jeremy Vizzini.
//  This software is released subject to licensing conditions as detailed in Licence.txt.
//

#import "NSImage+STL.h"


static
NSData *CGImageGetALPDataInRegion(CGImageRef imageRef, NSRect region) {
    int width = region.size.width;
    int height = region.size.height;
    int bitmapBytesPerRow = (width * 4);
    int bitmapByteCount = (bitmapBytesPerRow * height);
    
    CGColorSpaceRef colorspace = CGImageGetColorSpace(imageRef);
    CGContextRef context = CGBitmapContextCreate(NULL,
                                                 width,
                                                 height,
                                                 8,
                                                 bitmapBytesPerRow,
                                                 colorspace,
                                                 kCGImageAlphaPremultipliedLast);
    
    if (context == NULL)
        return nil;
    
    CGRect drawRegion;
    drawRegion.origin = CGPointZero;
    drawRegion.size.width = width;
    drawRegion.size.height = height;
    
    CGContextTranslateCTM(context,
                          - region.origin.x + (drawRegion.size.width  - region.size.width),
                          - region.origin.y - (drawRegion.size.height - region.size.height));
    CGContextDrawImage(context, drawRegion, imageRef);
    
    void *bitmapData = CGBitmapContextGetData(context);
    NSData *data = [NSData dataWithBytes:bitmapData length:bitmapByteCount];
    CGContextRelease(context);
    
    return data;
}

static
NSData *CGImageGetALPData(CGImageRef imageRef) {
    NSRect region = CGRectZero;
    region.size.width = CGImageGetWidth(imageRef);
    region.size.height = CGImageGetHeight(imageRef);
    return CGImageGetALPDataInRegion(imageRef, region);
}

@implementation NSImage (STL)

#pragma mark Properties

- (NSRect)rect {
    return NSMakeRect(0.0, 0.0, self.size.width, self.size.height);
}

#pragma mark Transformation

- (NSImage *)resizedImageInSize:(NSSize)size {
    CGFloat scale = MIN(size.width / self.size.width,
                        size.height / self.size.height);
    return [self scaledImageByFactor:scale];
}

- (NSImage *)scaledImageByFactor:(CGFloat)scale {
    if (scale == 1.0) {
        return self;
    }
    
    NSRect destRect = NSMakeRect(0, 0,
                                (int)(self.size.width * scale),
                                (int)(self.size.height * scale));
        
    NSImage *newImage = [[NSImage alloc] initWithSize:destRect.size];
    [newImage lockFocus];
    [[NSGraphicsContext currentContext] setImageInterpolation:NSImageInterpolationHigh];
    [self drawInRect:destRect];
    [newImage unlockFocus];
    return newImage;
}

#pragma mark Data

- (NSData *)ALPData {
    return CGImageGetALPData([self CGImage]);
}

- (NSData *)ALPDataInRegion:(NSRect)region {
    return CGImageGetALPDataInRegion([self CGImage], region);
}

#pragma mark Write

- (BOOL)writeAsPNGToFileURL:(NSURL *)fileURL {
    CGImageRef imageRef = [self CGImageForProposedRect:nil context:nil hints:nil];
    NSBitmapImageRep *imageRep = [[NSBitmapImageRep alloc] initWithCGImage:imageRef];
    NSData *pngData = [imageRep representationUsingType:NSPNGFileType properties:@{}];
    return [pngData writeToURL:fileURL atomically:YES];
}

#pragma mark Quartz

- (CGImageRef)CGImage {
    return [self CGImageForProposedRect:nil context:nil hints:nil];
}

@end

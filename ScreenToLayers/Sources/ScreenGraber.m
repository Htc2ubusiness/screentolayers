//
//  ScreenGraber.m
//  ScreenToLayers
//
//  Created by Jeremy Vizzini.
//  This software is released subject to licensing conditions as detailed in Licence.txt.
//

#import "ScreenGraber.h"
#import "GrabbedWindow.h"

#pragma mark - Helpers

static inline
void ChangeBits(uint32_t *currentBits, uint32_t flagsToChange, BOOL setFlags) {
    if(setFlags) {
        *currentBits = *currentBits | flagsToChange;
    } else {
        *currentBits = *currentBits & ~flagsToChange;
    }
}

#pragma mark - ScreenGraber Private

@interface ScreenGraber ()

@property (assign) CGWindowListOption listOptions;
@property (assign) CGWindowListOption windowOptions;
@property (assign) CGWindowImageOption imageOptions;
@property (strong) NSMutableArray<GrabbedWindow *> *windowsList;

@end

#pragma mark - ScreenGraber Implementation

@implementation ScreenGraber

- (instancetype)init {
    self = [super init];
    if (self) {
        self.imageBounds = CGRectInfinite;
        self.listOptions = kCGWindowListOptionOnScreenOnly;
        self.imageOptions = 0;
        self.windowOptions = kCGWindowListOptionIncludingWindow;
        self.windowsList = [NSMutableArray array];
        self.useBestResolution = YES;
    }
    return self;
}

#pragma mark Windows list

static
void WindowListApplierFunction(const void *inputDictionary, void *context)
{
    NSDictionary *entry = (__bridge NSDictionary*)inputDictionary;
    ScreenGraber *graber = (__bridge ScreenGraber *)context;
    int sharingState = [entry[(id)kCGWindowSharingState] intValue];
    if (sharingState == kCGWindowSharingNone) {
        return;
    }
    
    GrabbedWindow *window = [[GrabbedWindow alloc] init];
    window.processID = [entry[(id)kCGWindowOwnerPID] integerValue];
    window.windowID = [entry[(id)kCGWindowNumber] unsignedIntValue];
    window.level = [entry[(id)kCGWindowLayer] integerValue];
    window.ownerName = entry[(id)kCGWindowOwnerName];
    window.title = entry[(id)kCGWindowName];
    window.tag = graber.windowsList.count;
    
    CGRect windowBounds;
    CFDictionaryRef boundsDict = (__bridge CFDictionaryRef)entry[(id)kCGWindowBounds];
    CGRectMakeWithDictionaryRepresentation(boundsDict, &windowBounds);
    window.bounds = windowBounds;
    
    [graber.windowsList addObject:window];
}

- (CFArrayRef)newWindowListFromSelectedObjects:(NSArray *)selectedObjects {
    NSArray *sortDescriptors = @[[[NSSortDescriptor alloc] initWithKey:@"tag" ascending:YES]];
    NSArray *sortedSelection = [selectedObjects sortedArrayUsingDescriptors:sortDescriptors];
    
    NSInteger i = 0, count = [sortedSelection count];
    CGWindowID windowIDs[count];
    for (GrabbedWindow *window in sortedSelection) {
        windowIDs[i++] = window.windowID;
    }
    
    CFArrayRef windowIDsArray = CFArrayCreate(kCFAllocatorDefault,
                                              (const void **)windowIDs,
                                              [sortedSelection count],
                                              NULL);
    return windowIDsArray;
}

- (void)updateWindows {
    CFArrayRef cfWindowList = CGWindowListCopyWindowInfo(self.listOptions, kCGNullWindowID);
    [self.windowsList removeAllObjects];
    CFArrayApplyFunction(cfWindowList,
                         CFRangeMake(0, CFArrayGetCount(cfWindowList)),
                         &WindowListApplierFunction,
                         (__bridge void *)(self));
    CFRelease(cfWindowList);
}

- (NSArray<GrabbedWindow *> *)windows {
    return self.windowsList;
}

#pragma mark Grab screenshot

- (NSImage *)screenshot {
    return [self screenshotForBounds:CGRectInfinite];
}

- (NSImage *)screenshotForBounds:(NSRect)bounds {
    CGImageRef imageRef = CGWindowListCreateImage(bounds,
                                                  kCGWindowListOptionOnScreenOnly,
                                                  kCGNullWindowID,
                                                  kCGWindowImageDefault);
    NSImage *image = [[NSImage alloc] initWithCGImage:imageRef size:NSZeroSize];
    CGImageRelease(imageRef);
    return image;
}

- (NSImage *)screenshotForWindowIndex:(NSInteger)index {
    GrabbedWindow *window = self.windowsList[index];
    CGImageRef imageRef = CGWindowListCreateImage(self.imageBounds,
                                                  self.windowOptions,
                                                  window.windowID,
                                                  self.imageOptions);
    NSImage *image = [[NSImage alloc] initWithCGImage:imageRef size:NSZeroSize];
    CGImageRelease(imageRef);
    return image;
}

- (NSImage *)screenshotForWindowsSelection:(NSIndexSet *)selection {
    NSArray *objects = [self.windowsList objectsAtIndexes:selection];
    CFArrayRef windowIDs = [self newWindowListFromSelectedObjects:objects];
    CGImageRef imageRef = CGWindowListCreateImageFromArray(self.imageBounds,
                                                           windowIDs,
                                                           self.imageOptions);
    NSImage *image = [[NSImage alloc] initWithCGImage:imageRef size:NSZeroSize];
    CGImageRelease(imageRef);
    return image;
}

#pragma mark Settings

- (void)setOffscreenWindowsEnabled:(BOOL)flag {
    ChangeBits(&_listOptions, kCGWindowListOptionOnScreenOnly, !flag);
}

- (BOOL)isOffscreenWindowsEnabled {
    return !(_listOptions & kCGWindowListOptionOnScreenOnly);
}

- (void)setDesktopWindowsEnabled:(BOOL)flag {
    ChangeBits(&_listOptions, kCGWindowListExcludeDesktopElements, !flag);
}

- (BOOL)isDesktopWindowsEnabled {
    return !(_listOptions & kCGWindowListExcludeDesktopElements);
}

- (void)setFramingEffectsEnabled:(BOOL)flag {
    ChangeBits(&_imageOptions, kCGWindowImageBoundsIgnoreFraming, !flag);
}

- (BOOL)isFramingEffectsEnabled {
    return !(_imageOptions & kCGWindowImageBoundsIgnoreFraming);
}

- (void)setImageOpaque:(BOOL)flag {
    ChangeBits(&_imageOptions, kCGWindowImageShouldBeOpaque, flag);
}

- (BOOL)isImageOpaque {
    return (_imageOptions & kCGWindowImageShouldBeOpaque);
}

- (void)setShadowsOnly:(BOOL)flag {
    ChangeBits(&_imageOptions, kCGWindowImageOnlyShadows, flag);
}

- (BOOL)isShadowsOnly {
    return (_imageOptions & kCGWindowImageOnlyShadows);
}

- (void)setTightFit:(BOOL)flag {
    _imageBounds = (flag) ? CGRectNull : CGRectInfinite;
}

- (BOOL)isTightFit {
    return CGRectIsNull(_imageBounds);
}

- (void)setSingleWindowOptions:(SingleWindowOptions)windowOptions {
    CGWindowListOption options = 0;
    switch(windowOptions) {
        case SingleWindowOptionsAboveOnly:
            options = kCGWindowListOptionOnScreenAboveWindow;
            break;
        case SingleWindowOptionsAboveIncluded:
            options = kCGWindowListOptionOnScreenAboveWindow | kCGWindowListOptionIncludingWindow;
            break;
        case SingleWindowOptionsOnly:
            options = kCGWindowListOptionIncludingWindow;
            break;
        case SingleWindowOptionsBelowIncluded:
            options = kCGWindowListOptionOnScreenBelowWindow | kCGWindowListOptionIncludingWindow;
            break;
        case SingleWindowOptionsBelowOnly:
            options = kCGWindowListOptionOnScreenBelowWindow;
            break;
        default:
            break;
    }
    self.windowOptions = options;
}

@end

//
//  PSDWriter.m
//  ScreenToLayers
//
//  Modified by Jérémy Vizzini
//  Forked from : https://github.com/bengotow/PSDWriter
//  This software is released subject to licensing conditions as detailed in Licence.txt.
//

#import "PSDWriter.h"
#import "NSData+STL.h"
#import "PSDLayer.h"
#import "NSImage+STL.h"

#pragma mark PSDWriter private

@interface PSDWriter ()

@property (strong) NSMutableArray *layers;
@property (strong) NSData *previewData;

@property (strong) NSData *emptyRowData;
@property (strong) NSData *packedEmptyRowData;
@property (strong) NSData *packedPreviewData;
@property (strong) NSMutableArray *packedChannels;

@end

#pragma mark PSDWriter implementation

@implementation PSDWriter

- (instancetype)init {
    return [self initWithSize:NSMakeSize(512.0, 512.0)];
}

- (instancetype)initWithSize:(CGSize)size {
    self = [super init];
    if (self){
        _size = size;
        _layers = [[NSMutableArray alloc] init];
        
        int transparentRowSize = sizeof(Byte) * (int)ceilf(_size.width * 4);
        _emptyRowData = [NSMutableData dataWithLength:transparentRowSize];
        _packedEmptyRowData = [_emptyRowData packedBitsWithSkip:4];
    }
    return self;
}

#pragma mark Images

- (void)setPreview:(NSImage *)preview {
    _previewData = [preview ALPData];
}

- (void)addImage:(NSImage *)image name:(NSString*)name offset:(CGPoint)offset {
    CGRect imageRegion = image.rect;
    CGRect screenRegion = imageRegion;
    screenRegion.origin = offset;
    
    if (NSMaxX(screenRegion) > _size.width) {
        imageRegion.size.width = _size.width - screenRegion.origin.x;
        screenRegion.size.width = imageRegion.size.width;
    }
    if (NSMaxY(screenRegion) > _size.height) {
        imageRegion.size.height = _size.height - screenRegion.origin.y;
        screenRegion.size.height = imageRegion.size.height;
    }
    if (NSMinX(screenRegion) < 0) {
        imageRegion.origin.x = fabs(screenRegion.origin.x);
        screenRegion.origin.x = 0;
        screenRegion.size.width = imageRegion.size.width - imageRegion.origin.x;
        imageRegion.size.width = screenRegion.size.width;
    }
    if (NSMinY(screenRegion) < 0) {
        imageRegion.origin.y = fabs(screenRegion.origin.y);
        screenRegion.origin.y = 0;
    }
    
    PSDLayer *layer = [[PSDLayer alloc] init];
    layer.imageData = [image ALPDataInRegion:imageRegion];
    layer.rect = screenRegion;
    layer.name = name;
    [_layers addObject:layer];
}

#pragma mark Data

static char signature8BPS[4] = {'8','B','P','S'};
static char signature8BIM[4] = {'8','B','I','M'};
static char blendModeKey[4]  = {'n','o','r','m'};
static Byte alphaChannel[2]  = { 0xFF, 0xFF };
static Byte resBytes[16] = {
    0x00, 0x48, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01,
    0x00, 0x48, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01
};

- (NSData *)packedDataForPreview {
    int capacity = _size.height * 4 * 2;
    NSMutableData *byteCounts = [NSMutableData dataWithCapacity:capacity];
    NSMutableData *scanlines = [NSMutableData data];
    
    int imageRowBytes = _size.width * 4;
    for (int channel = 0; channel < 4; channel++) {
        for (int row = 0; row < _size.height; row++) {
            @autoreleasepool {
                int start = row * imageRowBytes + channel;
                NSRange packRange = NSMakeRange(start, imageRowBytes);
                NSData *packed = [_previewData packedBitsForRange:packRange skip:4];
                [byteCounts appendValue:packed.length withLength:2];
                [scanlines appendData:packed];
            }
        }
    }
    
    NSMutableData *data = [NSMutableData data];
    [data appendData:byteCounts];
    [data appendData:scanlines];
    return data;
}

- (NSArray *)packedDataForLayer:(PSDLayer *)layer {
    NSMutableArray *layerChannels = [NSMutableArray array];
    NSData *imageData = layer.imageData;
    CGRect bounds = layer.floorRect;
    
    int imageRowBytes = bounds.size.width * 4;
    NSRange leftPackRange = NSMakeRange(0, bounds.origin.x * 4);
    NSData *packedLeft = [_emptyRowData packedBitsForRange:leftPackRange skip:4];
    NSRange rightPackRange = NSMakeRange(0, (_size.width - NSMaxX(bounds)) * 4);
    NSData *packedRight = [_emptyRowData packedBitsForRange:rightPackRange skip:4];
    
    for (int channel = 0; channel < 4; channel++) {
        NSMutableData *byteCounts, *scanlines;
        byteCounts = [NSMutableData dataWithCapacity:_size.height * 4 * 2];
        scanlines = [NSMutableData data];
        
        for (int row = 0; row < _size.height; row++) {
            @autoreleasepool {
                if (row < NSMinY(bounds) || row >= NSMaxY(bounds)) {
                    [byteCounts appendValue:_packedEmptyRowData.length withLength:2];
                    [scanlines appendData:_packedEmptyRowData];
                    continue;
                }
                
                int byteCount = 0;
                if (bounds.origin.x > 0.0) {
                    [scanlines appendData:packedLeft];
                    byteCount += packedLeft.length;
                }
                
                int start = (row - bounds.origin.y) * imageRowBytes + channel;
                NSRange packRange = NSMakeRange(start, imageRowBytes);
                NSData *packed = [imageData packedBitsForRange:packRange skip:4];
                [scanlines appendData:packed];
                byteCount += [packed length];
                
                if (NSMaxX(bounds) < _size.width) {
                    [scanlines appendData:packedRight];
                    byteCount += packedRight.length;
                }
                
                [byteCounts appendValue:byteCount withLength:2];
            }
        }
        
        NSMutableData *channelData = [[NSMutableData alloc] init];
        [channelData appendValue:1 withLength:2]; // write channel compression format
        [channelData appendData:byteCounts];      // write channel byte counts
        [channelData appendData:scanlines];       // write channel scanlines
        [layerChannels addObject:channelData];
    }
    return layerChannels;
}

- (void)computePackData {
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    queue.qualityOfService = NSOperationQualityOfServiceUserInitiated;
    
    _packedChannels = [NSMutableArray array];
    for (int i = 0; i < 4 * _layers.count; i++) {
        [_packedChannels addObject:[NSNull null]];
    }
    
    [queue addOperationWithBlock:^{
        _packedPreviewData = [self packedDataForPreview];
    }];

    for (int i = 0; i < _layers.count; i++) {
        [queue addOperationWithBlock:^{
            PSDLayer *layer = _layers[i];
            NSArray *data = [self packedDataForLayer:layer];
            for (int j = 0; j < data.count; j++) {
                _packedChannels[data.count * i + j] = data[j];
            }
        }];
    }
    
    [queue waitUntilAllOperationsAreFinished];
}

- (void)clearPackData {
    _packedChannels = nil;
    _packedPreviewData = nil;
}

- (NSString *)normalizePSDName:(NSString *)name {
    NSMutableString *asciiName = [[NSMutableString alloc] initWithString:name];
    CFStringTransform((CFMutableStringRef)asciiName, nil, kCFStringTransformToLatin, 0);
    CFStringTransform((CFMutableStringRef)asciiName, nil, kCFStringTransformStripCombiningMarks, 0);
    return [asciiName stringByPaddingToLength:15 withString:@" " startingAtIndex:0];
}

- (void)appendHeaderToData:(NSMutableData *)data {
    [data appendBytes:&signature8BPS length:4];           // signature
    [data appendValue:1 withLength:2];                    // version number
    [data appendValue:0 withLength:6];                    // reserved space
    [data appendValue:4 withLength:2];                    // nb of channels
    [data appendValue:_size.height withLength:4];         // height
    [data appendValue:_size.width withLength:4];          // width
    [data appendValue:8 withLength:2];                    // bits per channel
    [data appendValue:3 withLength:2];                    // color mode (3 = RGB)
    [data appendValue:0 withLength:4];                    // color data section
    
    NSMutableData *imageResources = [NSMutableData data]; // resources
    [imageResources appendBytes:&signature8BIM length:4];
    [imageResources appendValue:1005 withLength:2];
    [imageResources appendValue:0 withLength:2];
    [imageResources appendValue:16 withLength:4];
    [imageResources appendBytes:&resBytes length:16];
    
    [imageResources appendBytes:&signature8BIM length:4]; // layer structure
    [imageResources appendValue:1024 withLength:2];
    [imageResources appendValue:0 withLength:2];
    [imageResources appendValue:2 withLength:4];
    [imageResources appendValue:0 withLength:2];          // current layer = 0
    
    [data appendValue:imageResources.length withLength:4];
    [data appendData:imageResources];
}

- (void)appendLayersToData:(NSMutableData *)data {
    NSMutableData *layerInfo = [[NSMutableData alloc] init];
    [layerInfo appendValue:_layers.count withLength:2];
    
    for (int i = 0; i < _layers.count; i++) {
        PSDLayer *layer = self.layers[i];
        [layerInfo appendValue:0 withLength:4];             // top left bottom right 4x4
        [layerInfo appendValue:0 withLength:4];
        [layerInfo appendValue:_size.height withLength:4];
        [layerInfo appendValue:_size.width withLength:4];
        
        [layerInfo appendValue:4 withLength:2];            // number of channels
        for (int c = 0; c < 3; c++) {
            NSData *channelData = _packedChannels[4 * i + c];
            [layerInfo appendValue:c withLength:2];
            [layerInfo appendValue:channelData.length withLength:4];
        }
        
        [layerInfo appendBytes:&alphaChannel length:2];
        NSData *alphaData = _packedChannels[4 * i + 3];
        [layerInfo appendValue:alphaData.length withLength:4];
        
        [layerInfo appendBytes:&signature8BIM length:4];   // blend mode signature
        [layerInfo appendBytes:&blendModeKey length:4];    // blend type
        
        [layerInfo appendValue:255 withLength:1];          // opacity
        [layerInfo appendValue:0 withLength:1];            // clipping
        [layerInfo appendValue:1 withLength:1];            // layer invisible
        [layerInfo appendValue:0 withLength:1];
        [layerInfo appendValue:4+4+16 withLength:4];       // extra data length
        
        [layerInfo appendValue:0 withLength:4];            // mask info and layer name
        [layerInfo appendValue:0 withLength:4];
        
        NSString *layerName = [self normalizePSDName:layer.name];
        NSStringEncoding enconding = NSStringEncodingConversionAllowLossy;
        const char *layerNameCString = [layerName cStringUsingEncoding:enconding];
        [layerInfo appendValue:layerName.length withLength:1];
        [layerInfo appendBytes:layerNameCString length:layerName.length];
    }
    
    for (int i = 0; i < _packedChannels.count; i++) {
        [layerInfo appendData:_packedChannels[i]];
    }
    
    if ([layerInfo length] % 2 != 0) {                    // round divisible by 2.
        [layerInfo appendValue:0 withLength:1];
    }
    [data appendValue:layerInfo.length+4 withLength:4];   // length of layer and mask
    [data appendValue:layerInfo.length withLength:4];     // actual layer info
    [data appendData:layerInfo];
}

- (void)appendImageToData:(NSMutableData *)data {
    [data appendValue:1 withLength:2]; // compression format = 1 = RLE
    [data appendData:_packedPreviewData];
}

- (NSData *)createPSDData {
    if (_previewData == nil  || _layers.count == 0) {
        NSString *reason = @"Please provide layers and preview";
        @throw [NSException exceptionWithName:NSGenericException
                                       reason:reason
                                     userInfo:nil];
    }
    
    NSMutableData *data = [NSMutableData data];
    [self computePackData];
    [self appendHeaderToData:data];
    [self appendLayersToData:data];
    [self appendImageToData:data];
    [self clearPackData];
    return data;
}

@end

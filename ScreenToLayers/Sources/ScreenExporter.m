//
//  ScreenExporter.m
//  ScreenToLayers
//
//  Created by Jeremy Vizzini.
//  This software is released subject to licensing conditions as detailed in Licence.txt.
//

#import "ScreenExporter.h"
#import "ScreenGraber.h"
#import "GrabbedWindow.h"
#import "PSDWriter.h"
#import "NSImage+STL.h"

#pragma mark - ScreenExporter Private

@interface ScreenExporter ()

@property (strong) PSDWriter *psdWriter;
@property (strong) ScreenGraber *graber;
@property (strong) NSMutableArray *fileURLs;
@property (assign) NSInteger screenIndex;
@property (assign) CGRect screenFrame;
@property (assign) CGRect backingFrame;
@property (strong) NSString *name;
@property (strong) NSURL *dirURL;
@property (assign) CGFloat scale;
@property (assign) BOOL succeed;

@end

#pragma mark - ScreenExporter Implementation

@implementation ScreenExporter

#pragma mark Initializer

- (instancetype)init {
    self = [super init];
    if (self) {
        self.mergeAllScreens = NO;
        self.showCursor = YES;
        self.format = ScreenExportFormatPSD;
        self.autoOpenDirectory = NO;
        self.autoOpenScreenshot = NO;
        self.graber = [[ScreenGraber alloc] init];
        self.fileURLs = [NSMutableArray array];
    }
    return self;
}

#pragma mark Backing

- (CGFloat)computeMinScaleResolution {
    CGFloat scale = 100.0;
    for (NSScreen *screen in [NSScreen screens]) {
        scale = MIN(scale, screen.backingScaleFactor);
    }
    return scale;
}

- (CGFloat)computeMaxScaleResolution {
    CGFloat scale = 0.0;
    for (NSScreen *screen in [NSScreen screens]) {
        scale = MAX(scale, screen.backingScaleFactor);
    }
    return scale;
}

- (CGRect)computeBackingFrame {
    CGRect frame = CGRectZero;
    for (NSScreen *screen in [NSScreen screens]) {
        frame = CGRectUnion(frame, [self computeScreenFrame: screen]);
    }
    return frame;
}

- (CGRect)computeScreenFrame:(NSScreen *)screen {
    NSScreen *originScreen = [NSScreen screens][0];
    CGFloat shift = originScreen.frame.size.height - originScreen.frame.origin.x;
    NSRect frame = screen.frame;
    frame.origin.y = -screen.frame.size.height + shift - frame.origin.y;
    return frame;
}

- (NSScreen *)screenForRect:(NSRect)rect {
    CGFloat area = 0.0;
    NSScreen *screen = nil;
    for (NSScreen *curScreen in [NSScreen screens]) {
        NSRect croppedRect = NSIntersectionRect(rect, curScreen.frame);
        CGFloat croppedArea = croppedRect.size.width * croppedRect.size.height;
        if (croppedArea >= area) {
            area = croppedArea;
            screen = curScreen;
        }
    }
    return screen;
}

#pragma mark Screenshots

- (void)startWriting {
    if (self.format & ScreenExportFormatFolder) {
        NSURL *directoryURL = [self.dirURL URLByAppendingPathComponent:self.name];
        [[NSFileManager defaultManager] createDirectoryAtURL:directoryURL
                                 withIntermediateDirectories:YES
                                                  attributes:nil
                                                       error:nil];
    }
}

- (void)writePreview {
    NSImage *image = [_graber screenshotForBounds:self.screenFrame];
    
    if (self.format & ScreenExportFormatFolder) {
        NSURL *fileURL = [self.dirURL URLByAppendingPathComponent:self.name];
        NSString *layerName = [NSString stringWithFormat:@"%li-%@.png",
                               self.screenIndex, @"Preview"];
        fileURL = [fileURL URLByAppendingPathComponent:layerName];
        [self.fileURLs addObject:fileURL];
        [image writeAsPNGToFileURL:fileURL];
    }
    if (self.format & ScreenExportFormatPSD) {
        [self.psdWriter setPreview:image];
    }
}

- (void)writeWindowAtIndex:(NSInteger)index {
    GrabbedWindow *window = self.graber.windows[index];
    CGRect imageBounds = NSInsetRect(window.bounds, -40.0, -60.0);
    imageBounds = NSIntersectionRect(imageBounds, self.screenFrame);
    if (NSIsEmptyRect(imageBounds)) {
        return;
    }
    
    self.graber.imageBounds = imageBounds;
    NSImage *image = [self.graber screenshotForWindowIndex:index];
    CGFloat imageScale = image.size.width / imageBounds.size.width;
    image = [image scaledImageByFactor:self.scale / imageScale];
    
    if (self.format & ScreenExportFormatFolder) {
        NSURL *fileURL = [self.dirURL URLByAppendingPathComponent:self.name];
        NSString *layerName = [NSString stringWithFormat:@"%li-%li-%@.png",
            self.screenIndex, self.graber.windows.count - index, window.ownerName];
        fileURL = [fileURL URLByAppendingPathComponent:layerName];
        [self.fileURLs addObject:fileURL];
        [image writeAsPNGToFileURL:fileURL];
    }
    if (self.format & ScreenExportFormatPSD) {
        NSPoint offset = self.graber.imageBounds.origin;
        offset.x -= self.screenFrame.origin.x;
        offset.y += self.screenFrame.origin.y;
        offset.x *= self.scale, offset.y *= self.scale;
        [self.psdWriter addImage:image name:window.ownerName offset:offset];
    }
}

- (void)writeCursor {
    NSImage *image = [NSCursor currentCursor].image;
    
    if (self.format & ScreenExportFormatFolder) {
        NSURL *fileURL = [self.dirURL URLByAppendingPathComponent:self.name];
        NSString *layerName = [NSString stringWithFormat:@"%li-%li-%@.png",
                    self.screenIndex, self.graber.windows.count, @"Cursor"];
        fileURL = [fileURL URLByAppendingPathComponent:layerName];
        [self.fileURLs addObject:fileURL];
        [image writeAsPNGToFileURL:fileURL];
    }
    if (self.format & ScreenExportFormatPSD) {
        CGPoint offset = [NSEvent mouseLocation];
        offset = CGPointMake(offset.x * self.scale, offset.y * self.scale);
        offset.y = self.backingFrame.size.height * self.scale - offset.y;
        image = [image scaledImageByFactor:self.scale];
        [self.psdWriter addImage:image name:@"Cursor" offset:offset];
    }
}

- (void)finishWriting {
    if (self.format & ScreenExportFormatPSD) {
        NSData *psdData = [self.psdWriter createPSDData];
        if (!psdData) {
            NSLog(@"Cannot create PSD file from screen.");
            self.succeed = NO;
            return;
        }
        
        NSString *fileName = [NSString stringWithFormat:@"%@ %li.psd",
                              self.name, self.screenIndex];
        NSURL *fileURL = [self.dirURL URLByAppendingPathComponent:fileName];
        if (![psdData writeToURL:fileURL atomically:YES]) {
            NSLog(@"Cannot write PSD file at URL %@", fileURL);
            self.succeed = NO;
            return;
        }
        
        [self.fileURLs addObject:fileURL];
    }
}

- (void)writeAllLayers {
    [self startWriting];
    [self writePreview];
    for (NSInteger i = self.graber.windows.count - 1; i >= 0; i--) {
        [self writeWindowAtIndex:i];
    }
    if (self.showCursor) {
        [self writeCursor];
    }
    [self finishWriting];
}

#pragma mark Exportation

- (void)processSeparatedScreens {
    self.screenIndex = 0;
    self.backingFrame = [self computeBackingFrame];
    for (NSScreen *screen in [NSScreen screens]) {
        self.scale = screen.backingScaleFactor;
        self.screenFrame = [self computeScreenFrame:screen];
        self.screenIndex += 1;
        
        CGSize size = self.screenFrame.size;
        size.width *= self.scale, size.height *= self.scale;
        self.psdWriter = [[PSDWriter alloc] initWithSize:size];
        [self writeAllLayers];
        self.psdWriter = nil;
    }
}

- (void)processMergedScreens {
    self.screenIndex = 0;
    self.backingFrame = [self computeBackingFrame];
    self.scale = [self computeMaxScaleResolution];
    self.screenFrame = self.backingFrame;
    
    CGSize size = self.screenFrame.size;
    size.width *= self.scale, size.height *= self.scale;
    self.psdWriter = [[PSDWriter alloc] initWithSize:size];
    [self writeAllLayers];
    self.psdWriter = nil;
}

- (BOOL)writeToDirURL:(NSURL *)dirURL {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH-mm-ss";
    self.name = [dateFormatter stringFromDate:[NSDate date]];
    self.dirURL = dirURL;
    self.succeed = YES;
    
    [self.graber updateWindows];
    if (self.mergeAllScreens) {
        [self processMergedScreens];
    } else {
        [self processSeparatedScreens];
    }
    
    if (self.autoOpenDirectory) {
        NSWorkspace *ws = [NSWorkspace sharedWorkspace];
        [ws activateFileViewerSelectingURLs:self.fileURLs];
    }
    if (self.autoOpenScreenshot) {
        NSWorkspace *ws = [NSWorkspace sharedWorkspace];
        for (NSURL *url in self.fileURLs) {
            [ws openURL:url];
        }
    }
    
    [self.fileURLs removeAllObjects];
    return self.succeed;
}

- (void)writeToDirURL:(NSURL *)dirURL completion:(void(^)(BOOL))block {
    int queueId = DISPATCH_QUEUE_PRIORITY_HIGH;
    dispatch_async(dispatch_get_global_queue(queueId, 0), ^{
        BOOL result = [self writeToDirURL:dirURL];
        dispatch_async(dispatch_get_main_queue(), ^{
            block(result);
        });
    });
}

@end

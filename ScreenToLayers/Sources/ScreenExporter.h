//
//  ScreenExporter.h
//  ScreenToLayers
//
//  Created by Jeremy Vizzini.
//  This software is released subject to licensing conditions as detailed in Licence.txt.
//

#import <Cocoa/Cocoa.h>

typedef NS_ENUM(NSUInteger, ScreenExportFormat) {
    ScreenExportFormatPSD = 1,
    ScreenExportFormatFolder = 2
};

@interface ScreenExporter : NSObject

#pragma mark Layers

@property (readwrite, assign) BOOL showCursor;

#pragma mark Multiscreen

@property (readwrite, assign) BOOL mergeAllScreens;

#pragma mark Exportation

@property (readwrite, assign) ScreenExportFormat format;
@property (readwrite, assign) BOOL autoOpenDirectory;
@property (readwrite, assign) BOOL autoOpenScreenshot;

- (BOOL)writeToDirURL:(NSURL *)dirURL;
- (void)writeToDirURL:(NSURL *)dirURL completion:(void(^)(BOOL))block;

@end

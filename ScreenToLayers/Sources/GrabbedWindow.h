//
//  GrabbedWindow.h
//  ScreenToLayers
//
//  Created by Jeremy Vizzini.
//  This software is released subject to licensing conditions as detailed in Licence.txt.
//

#import <Foundation/Foundation.h>

@interface GrabbedWindow : NSObject

@property (readwrite, strong) NSString *ownerName;
@property (readwrite, strong) NSString *title;
@property (readwrite, assign) NSInteger processID;
@property (readwrite, assign) CGWindowID windowID;
@property (readwrite, assign) NSRect bounds;
@property (readwrite, assign) NSInteger level;
@property (readwrite, assign) NSInteger tag;

@end

//
//  NSImage+STL.h
//  ScreenToLayers
//
//  Created by Jeremy Vizzini.
//  This software is released subject to licensing conditions as detailed in Licence.txt.
//

#import <Cocoa/Cocoa.h>

@interface NSImage (STL)

#pragma mark Properties

@property (readonly, assign) NSRect rect;

#pragma mark Transformation

- (NSImage *)resizedImageInSize:(NSSize)size;
- (NSImage *)scaledImageByFactor:(CGFloat)factor;

#pragma mark Data

- (NSData *)ALPData;
- (NSData *)ALPDataInRegion:(NSRect)region;

#pragma mark Write

- (BOOL)writeAsPNGToFileURL:(NSURL *)fileURL;

#pragma mark Quartz

- (CGImageRef)CGImage;

@end

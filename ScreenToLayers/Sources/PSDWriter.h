//
//  PSDWriter.h
//  ScreenToLayers
//
//  Modified by Jérémy Vizzini
//  Forked from : https://github.com/bengotow/PSDWriter
//  This software is released subject to licensing conditions as detailed in Licence.txt.
//

#import <Cocoa/Cocoa.h>

@interface PSDWriter : NSObject

#pragma mark Initializers

- (instancetype)initWithSize:(CGSize)size NS_DESIGNATED_INITIALIZER;

#pragma mark Properties

@property (readonly, nonatomic, assign) CGSize size;

#pragma mark Images

- (void)setPreview:(NSImage *)preview;
- (void)addImage:(NSImage *)image
            name:(NSString *)name
          offset:(CGPoint)offset;

#pragma mark Data

- (NSData *)createPSDData;

@end

//
//  ListController.m
//  ScreenToLayers
//
//  Created by Jeremy Vizzini.
//  This software is released subject to licensing conditions as detailed in Licence.txt.
//

#import "ListController.h"
#import "ScreenGraber.h"
#import "NSImage+STL.h"
#import "GrabbedWindow.h"

#pragma mark - ListController Private

@interface ListController () <NSTableViewDelegate, NSTableViewDataSource>

@property (strong) ScreenGraber *graber;
@property (weak) IBOutlet NSTableView *tableView;
@property (weak) IBOutlet NSImageView *outputView;
@property (weak) IBOutlet NSButton *listOffscreenWindows;
@property (weak) IBOutlet NSButton *listDesktopWindows;
@property (weak) IBOutlet NSButton *imageFramingEffects;
@property (weak) IBOutlet NSButton *imageOpaqueImage;
@property (weak) IBOutlet NSButton *imageShadowsOnly;
@property (weak) IBOutlet NSButton *imageTightFit;
@property (weak) IBOutlet NSMatrix *singleWindow;

@end

#pragma mark - ListController Implementation

@implementation ListController

#pragma mark Singleton

+ (instancetype)sharedController {
    static ListController *controller = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        controller = [[self alloc] initWithWindowNibName:@"ListWindow"];
    });
    return controller;
}

#pragma mark Initializers

- (void)awakeFromNib {
    self.graber = [[ScreenGraber alloc] init];
    self.graber.tightFit = YES;
    
    [[self window] makeKeyAndOrderFront:self];
    [self performSelectorOnMainThread:@selector(updateWindowList)
                           withObject:self
                        waitUntilDone:NO];
    [self performSelectorOnMainThread:@selector(updateImageWithAllScreen)
                           withObject:self
                        waitUntilDone:NO];
}

#pragma mark Update data

- (void)updateWindowList {
    [self.graber updateWindows];
    [self.tableView reloadData];
}

- (void)updateImageWithSelection {
    NSImage *image = nil;
	NSIndexSet *selection = [self.tableView selectedRowIndexes];
    if([selection count] == 1) {
        image = [self.graber screenshotForWindowIndex:selection.firstIndex];
	} else if ([selection count] != 0){
        image = [self.graber screenshotForWindowsSelection:selection];
	}
    self.outputView.image = image;
}

- (void)updateImageWithAllScreen {
    self.outputView.image = [self.graber screenshot];
}

#pragma mark Actions

- (IBAction)toggleOffscreenWindows:(id)sender {
    self.graber.offscreenWindowsEnabled = ([sender intValue] == NSOnState);
	[self updateWindowList];
	[self updateImageWithSelection];
}

- (IBAction)toggleDesktopWindows:(id)sender {
    self.graber.desktopWindowsEnabled = ([sender intValue] == NSOnState);
	[self updateWindowList];
    [self updateImageWithSelection];
}

- (IBAction)toggleFramingEffects:(id)sender {
    self.graber.framingEffectsEnabled = ([sender intValue] == NSOffState);
	[self updateImageWithSelection];
}

- (IBAction)toggleOpaqueImage:(id)sender {
    self.graber.imageOpaque = ([sender intValue] == NSOnState);
	[self updateImageWithSelection];
}

- (IBAction)toggleShadowsOnly:(id)sender {
    self.graber.shadowsOnly = ([sender intValue] == NSOnState);
	[self updateImageWithSelection];
}

- (IBAction)toggleTightFit:(id)sender {
    self.graber.tightFit = ([sender intValue] == NSOnState);
	[self updateImageWithSelection];
}

- (IBAction)updateSingleWindowOption:(id)sender {
    [self.graber setSingleWindowOptions:[_singleWindow selectedRow]];
	[self updateImageWithSelection];
}

- (IBAction)grabScreenShot:(id)sender {
    NSIndexSet *selection = [[NSIndexSet alloc] init];
    [self.tableView selectRowIndexes:selection byExtendingSelection:NO];
	[self updateImageWithAllScreen];
}

- (IBAction)refreshWindowList:(id)sender {
	[self updateWindowList];
	[self updateImageWithSelection];
}

- (IBAction)exportImage:(id)sender {
    NSImage *image = self.outputView.image;
    if (!image) {
        NSBeep();
        return;
    }
    
    NSSavePanel *panel = [NSSavePanel savePanel];
    [panel setCanCreateDirectories:YES];
    [panel setAllowedFileTypes:@[@"png"]];
    [panel beginSheetModalForWindow:[self window]  completionHandler:^(NSInteger result) {
        if (result != NSFileHandlingPanelOKButton) {
            return;
        }
        [image writeAsPNGToFileURL:panel.URL];
     }];
}

#pragma mark NSTableViewDataSource

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return self.graber.windows.count;
}

- (id)tableView:(NSTableView *)tableView
objectValueForTableColumn:(NSTableColumn *)tableColumn
            row:(NSInteger)row
{
    if (row < 0 || row >= self.graber.windows.count) {
        return nil;
    }
    
    GrabbedWindow *window = self.graber.windows[row];
    if ([tableColumn.identifier isEqualToString:@"application"]) {
        return window.ownerName;
    } else if ([tableColumn.identifier isEqualToString:@"origin"]) {
        return [NSString stringWithFormat:@"%.f/%.f",
                window.bounds.origin.x,
                window.bounds.origin.y];
    } else if ([tableColumn.identifier isEqualToString:@"size"]) {
        return [NSString stringWithFormat:@"%.f/%.f",
                window.bounds.size.width,
                window.bounds.size.height];
    } else if ([tableColumn.identifier isEqualToString:@"windowID"]) {
        return [NSNumber numberWithInteger:window.windowID];
    } else if ([tableColumn.identifier isEqualToString:@"windowLevel"]) {
        return [NSNumber numberWithInteger:window.level];
    }
    return nil;
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification {
    [_singleWindow setEnabled:self.tableView.selectedRowIndexes.count <= 1];
    [self updateImageWithSelection];
}

#pragma mark NSTableViewDelegate


@end

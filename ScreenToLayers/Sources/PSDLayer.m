//
//  PSDLayer.m
//  ScreenToLayers
//
//  Forked from : https://github.com/bengotow/PSDWriter
//  This software is released subject to licensing conditions as detailed in Licence.txt.
//

#import "PSDLayer.h"

@implementation PSDLayer

- (NSRect)floorRect {
    NSRect bounds = self.rect;
    bounds.origin.x = floorf(bounds.origin.x);
    bounds.origin.y = floorf(bounds.origin.y);
    bounds.size.width = floorf(bounds.size.width);
    bounds.size.height = floorf(bounds.size.height);
    return bounds;
}

@end

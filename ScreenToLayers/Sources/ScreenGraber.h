//
//  ScreenGraber.h
//  ScreenToLayers
//
//  Created by Jeremy Vizzini.
//  This software is released subject to licensing conditions as detailed in Licence.txt.
//

@import Cocoa;

@class GrabbedWindow;

typedef NS_ENUM(NSUInteger, SingleWindowOptions) {
    SingleWindowOptionsAboveOnly     = 0,
    SingleWindowOptionsAboveIncluded = 1,
    SingleWindowOptionsOnly          = 2,
    SingleWindowOptionsBelowIncluded = 3,
    SingleWindowOptionsBelowOnly     = 4
};

@interface ScreenGraber : NSObject

#pragma mark Windows list

- (void)updateWindows;
@property (readonly, strong, nonatomic) NSArray<GrabbedWindow *> *windows;

#pragma mark Grab screenshots

- (NSImage *)screenshot;
- (NSImage *)screenshotForBounds:(NSRect)bounds;
- (NSImage *)screenshotForWindowIndex:(NSInteger)index;
- (NSImage *)screenshotForWindowsSelection:(NSIndexSet *)selection;

#pragma mark Settings

@property (nonatomic) NSRect imageBounds;
@property (nonatomic, getter=isTightFit) BOOL tightFit;

@property (nonatomic, getter=isImageOpaque) BOOL imageOpaque;
@property (nonatomic, getter=isShadowsOnly) BOOL shadowsOnly;
@property (nonatomic, getter=isDesktopWindowsEnabled) BOOL desktopWindowsEnabled;
@property (nonatomic, getter=isFramingEffectsEnabled) BOOL framingEffectsEnabled;
@property (nonatomic, getter=isOffscreenWindowsEnabled) BOOL offscreenWindowsEnabled;

@property (nonatomic) BOOL useBestResolution;
@property (nonatomic) BOOL useNominalResolution;

- (void)setSingleWindowOptions:(SingleWindowOptions)windowOptions;

@end
